package com.nevermore.remoter.util;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

public class EventExecuter {


    private static Robot robot = EventExecuter.getRobot();

    public static void execute(InputEvent event) {
        MouseEvent mevent;
        MouseWheelEvent mwevent;
        KeyEvent kevent;
        int mouseButtonMask;
        switch (event.getID()) {
            case MouseEvent.MOUSE_MOVED:
                mevent = (MouseEvent) event;
                robot.mouseMove(mevent.getX(), mevent.getY());
                break;
            case MouseEvent.MOUSE_PRESSED:
                mevent = (MouseEvent) event;
                robot.mouseMove(mevent.getX(), mevent.getY());
                mouseButtonMask = getMouseButtonMask(mevent.getButton());
                if (mouseButtonMask != -100) {
                    robot.mousePress(mouseButtonMask);
                }
                break;
            case MouseEvent.MOUSE_RELEASED:
                mevent = (MouseEvent) event;
                robot.mouseMove(mevent.getX(), mevent.getY());
                mouseButtonMask = getMouseButtonMask(mevent.getButton());
                if (mouseButtonMask != -100) {
                    robot.mouseRelease(mouseButtonMask);
                }
                break;
            case MouseEvent.MOUSE_WHEEL:
                mwevent = (MouseWheelEvent) event;
                robot.mouseWheel(mwevent.getWheelRotation());
                break;
            case MouseEvent.MOUSE_DRAGGED:
                mevent = (MouseEvent) event;
                robot.mouseMove(mevent.getX(), mevent.getY());
                break;

            case KeyEvent.KEY_PRESSED:
                kevent = (KeyEvent) event;
                robot.keyPress(kevent.getKeyCode());
                break;
            case KeyEvent.KEY_RELEASED:
                kevent = (KeyEvent) event;
                robot.keyRelease(kevent.getKeyCode());
                break;

            default:
                break;
        }
    }

    private static Robot getRobot() {
        if (robot == null) {
            try {
                robot = new Robot();
            } catch (AWTException e) {
                LogUtils.error(e);
            }
        }
        return robot;
    }

    private static int getMouseButtonMask(int button) {
        if (button == MouseEvent.BUTTON1)
            return InputEvent.BUTTON1_MASK;
        if (button == MouseEvent.BUTTON3)
            return InputEvent.BUTTON3_MASK;
        return -100;
    }
}
