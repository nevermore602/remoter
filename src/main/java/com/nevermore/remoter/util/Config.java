package com.nevermore.remoter.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Config {
    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");

    public static String dateTimeFormat(Date date) {
        return dateTimeFormat.format(date);
    }

    public static String timeFormat(Date date) {
        return timeFormat.format(date);
    }

    public static String dateTimeFormat(String format, Date date) {
        return new SimpleDateFormat(format).format(date);
    }
}