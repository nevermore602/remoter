package com.nevermore.remoter.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ScreenShot {

    private static Toolkit toolkit = Toolkit.getDefaultToolkit();
    private static Robot robot;

    static {
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public static synchronized byte[] getScreenShotData() {
        byte[] bt = null;
        BufferedImage screenShot = getScreenShotImage();
        if (screenShot != null) {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            try {
                ImageIO.write(screenShot, "jpg", bout);
                bt = bout.toByteArray();
            } catch (IOException e) {
                LogUtils.warn(e);
            } finally {
                IOUtils.closeQuietly(bout);
            }
        }
        return bt;
    }

    private static synchronized BufferedImage getScreenShotImage() {
        Dimension dim = toolkit.getScreenSize();
        return robot.createScreenCapture(new Rectangle(0, 0, (int) dim.getWidth(), (int) dim.getHeight()));
    }
}
