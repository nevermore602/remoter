package com.nevermore.remoter.util;

import java.io.*;
import java.util.Date;


public class LogUtils {
    private static PrintWriter out = null;
    private static String dateStr = null;

    static {
        try {
            File logFolder = new File(Constant.LOG_PATH);
            if (!(logFolder.exists() && logFolder.isDirectory())) {
                if (!logFolder.mkdirs()) {
                    System.err.println("创建日志目录失败！");
                }
            }
            dateStr = Config.dateTimeFormat("yyyy_MM_dd", new Date());
            String logName = "app_" + dateStr + ".log";
            out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(Constant.LOG_PATH + "/" + logName, true)), true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void info(String message, Object... args) {
        log(String.format(message, args), 2, null);
    }

    public static void warn(String message, Object... args) {
        log(String.format(message, args), 1, null);
    }

    public static void warn(String message, Throwable e) {
        log(message, 1, e);
    }

    public static void warn(Throwable e) {
        warn(null, e);
    }

    public static void error(String message, Throwable e) {
        log(message, 0, e);
    }

    public static void error(Throwable e) {
        error("", e);
    }

    public static void error(String message, Object... args) {
        log(String.format(message, args), 0, null);
    }

    public static void debug(String message, Object... args) {
        log(String.format(message, args), 3, null);
    }

    /**
     * 记录日志方法
     *
     * @param message 日志内容
     * @param type    0: error; 1: warn; 2: info; 3: debug;
     * @param e       error级别日志时传入的异常对象
     */
    private static void log(String message, int type, Throwable e) {
        if (message == null) {
            message = "";
        }
        String head = "";
        switch (type) {
            case 0:
                head = "ERROR";
                break;
            case 1:
                head = "WARN";
                break;
            case 2:
                head = "INFO";
                break;
            case 3:
                head = "DEBUG";
                break;
        }
        try {
            String nowDateStr = Config.dateTimeFormat("yyyy_MM_dd", new Date());
            if (!nowDateStr.equals(dateStr)) {
                dateStr = nowDateStr;
                IOUtils.closeQuietly(out);
                out = new PrintWriter(new OutputStreamWriter(new FileOutputStream("log/" + "log_" + dateStr + ".log", true)), true);
            }
            String msg = String.format("[%s]-[%5.5s]-[%16.16s] %s", Config.timeFormat(new Date()), head,
                    trunckHeadIfOverLength(Thread.currentThread().getName(), 16), message);
            out.println(msg);
            System.out.println(msg);
            if ((type == 0 || type == 1) && e != null) {
                e.printStackTrace(out);
                e.printStackTrace(System.out);
            }

        } catch (FileNotFoundException fe) {
            fe.printStackTrace();
        }
    }

    private static String trunckHeadIfOverLength(String str, int maxLen) {
        if (str == null || str.length() <= maxLen) {
            return str;
        }
        return str.substring(str.length() - maxLen, str.length());
    }

    public static void main(String[] args) {
        LogUtils.warn(new RuntimeException("hehehe"));
        LogUtils.info("----------------------------------------");
        LogUtils.warn("name", new RuntimeException("hehehe"));
        LogUtils.info("----------------------------------------");
        LogUtils.warn("my name is %s", "hehehe");
    }
}
