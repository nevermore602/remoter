package com.nevermore.remoter.util;

import com.google.common.util.concurrent.*;

import java.util.concurrent.*;

public class TaskUtils {
    private static final ThreadFactory taskExecutorThreadFactory = new ThreadFactoryBuilder()
            .setNameFormat("task-executor-%d").setDaemon(true)
            .build();

    private static final ListeningExecutorService executorService =
            MoreExecutors.listeningDecorator(new ThreadPoolExecutor(100, 100,
                    10L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue<>(300),
                    taskExecutorThreadFactory));


    public static <T> void executeWithCallback(Callable<T> task, FutureCallback<T> callback) {
        Futures.addCallback(executorService.submit(task), callback);
    }

    public static void executeWithCallback(Runnable task, FutureCallback<Object> callback) {
        Futures.addCallback(executorService.submit(task), callback);
    }

    public static void execute(Runnable task) {
        executorService.execute(task);
    }

    public static void main(String[] args) {
        TaskUtils.executeWithCallback(
                (Runnable) () -> {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    throw new RuntimeException("test");
                },
                new FutureCallback<Object>() {
                    @Override
                    public void onSuccess(Object result) {
                        LogUtils.info("onSuccess");
                        LogUtils.info(result.toString());
                    }

                    @Override

                    public void onFailure(Throwable t) {
                        LogUtils.info("onFailure");
                        t.printStackTrace();
                    }
                }
        );
    }

}
