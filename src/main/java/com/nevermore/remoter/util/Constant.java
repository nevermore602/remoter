package com.nevermore.remoter.util;

public class Constant {
    public static String serverName = "REMOTECONTROLLER";
    public static int serverPort = 9999;

    public static String YES = "yes";
    public static String NO = "no";

    public static final String LOG_PATH = "log";
}
