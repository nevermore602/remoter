package com.nevermore.remoter.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.*;

public final class ZipUtils {

    // gzip压缩
    public static byte[] gzip(byte[] bytes) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = null;
        try {
            gzip = new GZIPOutputStream(out);
            gzip.write(bytes);
        } finally {
            IOUtils.closeQuietly(gzip);
        }
        byte[] zipedBytes = out.toByteArray();
        LogUtils.info(String.format("[gzip] 压缩前：%d，压缩后：%d，压缩比：%s", bytes.length, zipedBytes.length,
                (bytes.length - zipedBytes.length) * 1.0 / bytes.length));
        return zipedBytes;
    }

    // gzip解压缩
    public static byte[] unGzip(byte[] zipedBytes) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPInputStream gzipIn = null;
        try {
            gzipIn = new GZIPInputStream(new ByteArrayInputStream(zipedBytes));
            byte[] buffer = new byte[1024];
            int offset;
            while ((offset = gzipIn.read(buffer)) != -1) {
                out.write(buffer, 0, offset);
            }
        } finally {
            IOUtils.closeQuietly(gzipIn);
        }
        byte[] bytes = out.toByteArray();
        LogUtils.info(String.format("[gzip] 解压前：%d，解压后：%d", zipedBytes.length, bytes.length));
        return bytes;
    }

    //zlib压缩
    public static byte[] zlib(byte[] data) {
        byte[] output;
        Deflater compresser = new Deflater();
        compresser.reset();
        compresser.setInput(data);
        compresser.finish();
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
        byte[] buf = new byte[1024];
        while (!compresser.finished()) {
            int i = compresser.deflate(buf);
            bos.write(buf, 0, i);
        }
        output = bos.toByteArray();
        compresser.end();
        LogUtils.info(String.format("[zlib] 压缩前：%d，压缩后：%d，压缩比：%s", data.length, output.length,
                (data.length - output.length) * 1.0 / data.length));
        return output;
    }

    //zlib解压缩
    public static byte[] unZlib(byte[] data) throws DataFormatException {
        byte[] output;
        Inflater decompresser = new Inflater();
        decompresser.reset();
        decompresser.setInput(data);

        ByteArrayOutputStream os = new ByteArrayOutputStream(data.length);
        byte[] buf = new byte[1024];
        while (!decompresser.finished()) {
            int i = decompresser.inflate(buf);
            os.write(buf, 0, i);
        }
        output = os.toByteArray();

        decompresser.end();
        LogUtils.info(String.format("[zlib] 解压前：%d，解压后：%d", data.length, output.length));
        return output;
    }
}
