package com.nevermore.remoter.server;

import com.nevermore.remoter.remote.RMIInterfaceImpl;
import com.nevermore.remoter.util.Constant;
import com.nevermore.remoter.util.LogUtils;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class RMIServer {

    private RMIInterfaceImpl remoteObject = null;
    private String serverURL = null;

    public RMIServer() {
        try {
            String host = InetAddress.getLocalHost().getHostAddress();  //获取本机地址
            /* jdk中的说明：
             * Naming 类提供在对象注册表中存储和获得远程对远程对象引用的方法。
             * Naming 类的每个方法都可将某个名称作为其一个参数，
             * 该名称是使用以下形式的 URL 格式（没有 scheme 组件）的 java.lang.String：
             * //host:port/name
             */
            serverURL = "//" + host + ":" + Constant.serverPort + "/" + Constant.serverName;

            /* jdk中的说明：
             * LocateRegistry 用于获得对特定主机（包括本地主机）上引导远程对象注册表的引用，
             * 或用于创建一个接受对特定端口调用的远程对象注册表。
             */
            LocateRegistry.createRegistry(Constant.serverPort);

            remoteObject = new RMIInterfaceImpl();
            new PermissionThread().start();

        } catch (Exception e) {
            LogUtils.error("服务初始化失败！", e);
        }

    }

    public void startService() {
        try {
            Naming.rebind(serverURL, remoteObject);//将服务类和URL绑定到命名空间
            LogUtils.info("RMI Server Started");
        } catch (Exception e) {
            LogUtils.error("启动服务失败！", e);
        }
    }

    public void stopService() {
        try {
            LogUtils.info("stopping service");
            Naming.unbind(serverURL);
        } catch (RemoteException | MalformedURLException | NotBoundException e) {
            e.printStackTrace();
        }
    }

    public RMIInterfaceImpl getRemoteObject() {
        return remoteObject;
    }

    private class PermissionThread extends Thread {
        ServerSocket ss;

        PermissionThread() throws Exception {
            ss = new ServerSocket(8888);
            this.setDaemon(true);
        }

        @Override
        public void run() {
            try {
                while (true) {
                    Socket socket = ss.accept();
                    OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
                    BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String requestHost;
                    while ((requestHost = br.readLine()) != null) {
                        int select = JOptionPane.showConfirmDialog(null,
                                "收到来自'" + requestHost + "'的连接请求，是否对该主机提供远程服务？",
                                "确认信息", JOptionPane.YES_NO_OPTION);
                        if (select == 0) { //选择Yes
                            osw.write(Constant.YES);
                            osw.flush();
                            break;
                        } else {
                            osw.write(Constant.NO);
                            osw.flush();
                            break;
                        }
                    }
                    osw.close();
                    br.close();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}