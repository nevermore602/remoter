package com.nevermore.remoter;

import com.nevermore.remoter.client.Client;

import javax.swing.*;

public class App {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            java.awt.EventQueue.invokeLater(() -> new Client().setVisible(true));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
