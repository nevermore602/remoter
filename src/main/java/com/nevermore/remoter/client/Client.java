package com.nevermore.remoter.client;

import com.nevermore.remoter.data.ConnectData;
import com.nevermore.remoter.remote.RMIInterface;
import com.nevermore.remoter.server.RMIServer;
import com.nevermore.remoter.util.ZipUtils;
import com.nevermore.remoter.util.Constant;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.rmi.Naming;
import java.util.Observable;
import java.util.Observer;

public class Client extends javax.swing.JFrame implements ActionListener, Observer {

    private javax.swing.JButton btStartServer;
    private javax.swing.JButton btConn;
    private javax.swing.JButton btDisconn;
    private javax.swing.JMenuItem itemStartServer;
    private javax.swing.JMenuItem itemConn;
    private javax.swing.JMenuItem itemDisconn;
    private javax.swing.JMenuItem itemExit;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JMenu menuFile;
    private ScreenPanel panelImage;
    private ConnectFrame cf;
    private JScrollPane scrollPane = null;
    private boolean isConnected = false;
    private RMIInterface rmi = null;
    private ConnectData connArg = null;
    private Image defaultImage = this.getToolkit().createImage(this.getClass().getClassLoader().getResource("images/bg5.jpg"));
    private boolean serverStarted = false;
    private RMIServer server;

    public Client() {
        initComponents();
        setTitle("远程控制器");
        panelImage.setImage(defaultImage);
        panelImage.setRmi(rmi);
        this.setLocationRelativeTo(null);
        server = new RMIServer();
    }

    private void initComponents() {
        jToolBar1 = new javax.swing.JToolBar();
        btStartServer = new javax.swing.JButton("启动服务",
                new ImageIcon(this.getClass().getClassLoader().getResource("images/start.png")));
        btConn = new javax.swing.JButton("建立连接",
                new ImageIcon(this.getClass().getClassLoader().getResource("images/connect.png")));
        btDisconn = new javax.swing.JButton("断开连接",
                new ImageIcon(this.getClass().getClassLoader().getResource("images/disconnect.png")));
        jMenuBar1 = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        itemConn = new javax.swing.JMenuItem();
        itemDisconn = new javax.swing.JMenuItem();
        itemExit = new javax.swing.JMenuItem();
        itemStartServer = new javax.swing.JMenuItem();

        panelImage = new ScreenPanel(null);

        scrollPane = new JScrollPane(panelImage, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jToolBar1.setRollover(true);
        btStartServer.setToolTipText("启动远程服务");
        btStartServer.setFocusable(false);
        btStartServer.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btStartServer.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(btStartServer);

        jToolBar1.add(new JToolBar.Separator());

        btConn.setToolTipText("建立连接");
        btConn.setFocusable(false);
        btConn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btConn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(btConn);

        btDisconn.setToolTipText("断开连接");
        btDisconn.setFocusable(false);
        btDisconn.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btDisconn.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(btDisconn);

        itemStartServer.addActionListener(this);
        itemConn.addActionListener(this);
        itemDisconn.addActionListener(this);
        btStartServer.addActionListener(this);
        btConn.addActionListener(this);
        btDisconn.addActionListener(this);
        itemExit.addActionListener(this);

        javax.swing.GroupLayout panelImageLayout = new javax.swing.GroupLayout(panelImage);
        panelImage.setLayout(panelImageLayout);
        panelImageLayout.setHorizontalGroup(
                panelImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 816, Short.MAX_VALUE)
        );
        panelImageLayout.setVerticalGroup(
                panelImageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 472, Short.MAX_VALUE)
        );

        menuFile.setText("系统");

        itemStartServer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.ALT_MASK));
        itemStartServer.setText("启动远程服务");
        menuFile.add(itemStartServer);

        itemConn.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK));
        itemConn.setText("建立连接");
        menuFile.add(itemConn);

        itemDisconn.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.ALT_MASK));
        itemDisconn.setText("断开连接");
        menuFile.add(itemDisconn);

        itemExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        itemExit.setText("退出");
        menuFile.add(itemExit);
        jMenuBar1.add(menuFile);
        setJMenuBar(jMenuBar1);

        Container container = getContentPane();
        container.setLayout(new BorderLayout());
        container.add(jToolBar1, BorderLayout.NORTH);
        container.add(scrollPane, BorderLayout.CENTER);

        cf = new ConnectFrame(this);
        cf.addObserver(this);

        pack();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == itemStartServer || e.getSource() == btStartServer) {
            startServer();
        }
        if (e.getSource() == itemConn || e.getSource() == btConn) {
            openConnection();
        }
        if (e.getSource() == itemDisconn || e.getSource() == btDisconn) {
            closeConnection();
        }
        if (e.getSource() == itemExit) {
            this.dispose();
            System.exit(0);
        }
    }

    private void startServer() {
        if (!serverStarted) {
            server.startService();
            server.getRemoteObject().setServerRunning(true);
            itemStartServer.setText("关闭远程服务");
            btStartServer.setToolTipText("关闭远程服务");
            btStartServer.setText("关闭服务");
            btStartServer.setIcon(new ImageIcon(this.getClass().getClassLoader().getResource("images/stop.png")));
            serverStarted = true;
        } else {
            server.stopService();
            server.getRemoteObject().setServerRunning(false);
            itemStartServer.setText("启动远程服务");
            btStartServer.setToolTipText("启动远程服务");
            btStartServer.setText("启动服务");
            btStartServer.setIcon(new ImageIcon(this.getClass().getClassLoader().getResource("images/start.png")));
            serverStarted = false;
        }
    }

    private void openConnection() {
        cf.setVisible();
    }

    private void closeConnection() {
        rmi = null;
        isConnected = false;
        btConn.setEnabled(true);
        itemConn.setEnabled(true);
        panelImage.setRmi(rmi);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof ConnectFrame) {
            if (arg instanceof ConnectData) {
                this.connArg = (ConnectData) arg;
                new ClientThread(connArg.getHost()).start();
            }
        }
    }

    private void controll() {
        if (isConnected) {
            new ScreenImageThread().start();
        }
    }

    private class ClientThread extends Thread {
        private String host;

        ClientThread(String host) {
            this.host = host;
            this.setDaemon(true);
        }

        @Override
        public void run() {
            try {
                Socket socket = new Socket(host, 8888);
                OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String local = InetAddress.getLocalHost().getHostName() + " (" + InetAddress.getLocalHost().getHostAddress() + ") ";
                osw.write(local + "\n");
                osw.flush();
                String response;
                while ((response = br.readLine()) != null) {
                    if (response.equals(Constant.YES)) {
                        rmi = (RMIInterface) Naming.lookup("//" + connArg.getHost() + ":" + connArg.getPort() + "/REMOTECONTROLLER");
                        btConn.setEnabled(false);
                        itemConn.setEnabled(false);
                        panelImage.setRmi(rmi);
                        isConnected = true;
                        //getInitImage();
                        controll();
                        break;
                    } else if (response.equals(Constant.NO)) {
                        JOptionPane.showMessageDialog(null, "连接失败！原因：目标主机拒绝了您的连接请求！", "提示", JOptionPane.WARNING_MESSAGE);
                        break;
                    }
                }
                osw.close();
                br.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "主机连接失败或该主机的远程服务未启动！", "连接失败", JOptionPane.ERROR_MESSAGE);
                cf.setVisible();
            }
        }
    }

    private class ScreenImageThread extends Thread {
        @Override
        public void run() {
            try {
                while (isConnected) {
                    byte[] bt = rmi.getImageData();
                    BufferedImage image;
                    if (bt != null) {
                        bt = ZipUtils.unZlib(bt);
                        InputStream in = new ByteArrayInputStream(bt);
                        image = ImageIO.read(in);
                        panelImage.setImage(image);
                    }
                    panelImage.repaint();
                    if ((rmi != null) && (!rmi.isServerRunning())) { //检查服务端的运行状态，如果关闭了服务就断开连接
                        closeConnection();
                    }
                    Thread.sleep(10);
                }
                panelImage.setImage(defaultImage);
                panelImage.repaint();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}