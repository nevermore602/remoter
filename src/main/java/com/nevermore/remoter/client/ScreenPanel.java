package com.nevermore.remoter.client;

import com.nevermore.remoter.remote.RMIInterface;
import com.nevermore.remoter.util.LogUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.rmi.RemoteException;

public class ScreenPanel extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener, KeyListener {
    private static final long serialVersionUID = 1L;
    private Image image;
    private RMIInterface rmi = null;

    ScreenPanel(Image image) {
        this.image = image;
        this.addKeyListener(this);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addMouseWheelListener(this);
    }

    void setImage(Image image) {
        this.image = image;
        //根据image来设置面板的预大小
        this.setPreferredSize(new Dimension(image.getWidth(this), image.getHeight(this)));
    }

    public void paintComponent(Graphics g) {
        g.drawImage(image, 0, 0, null);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        sendCommand(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        sendCommand(e);
    }

    @Override
    public void keyTyped(KeyEvent e) {
        sendCommand(e);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        /*
         * 点击该面板时，获取焦点才能够响应键盘事件☆
         */
        requestFocus();
        sendCommand(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        sendCommand(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        sendCommand(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        sendCommand(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        sendCommand(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        sendCommand(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        sendCommand(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        sendCommand(e);
    }

    private void sendCommand(InputEvent e) {
        if (rmi != null) {
            try {
                rmi.executeEvent(e);
            } catch (RemoteException e1) {
                LogUtils.error(e1);
            }
        }
    }

    void setRmi(RMIInterface rmi) {
        this.rmi = rmi;
    }
}