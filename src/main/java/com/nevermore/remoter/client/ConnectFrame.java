package com.nevermore.remoter.client;

import com.nevermore.remoter.data.ConnectData;
import com.nevermore.remoter.util.Constant;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

public class ConnectFrame extends Observable implements ActionListener {

    private JFrame frame;
    private javax.swing.JButton btCancel;
    private javax.swing.JButton btConn;
    private javax.swing.JTextField txtHost;

    ConnectFrame(JFrame parentFrame) {
        initComponents(parentFrame);
    }

    private void initComponents(JFrame parentFrame) {
        frame = new JFrame("建立连接");
        frame.setResizable(false);
        JLabel jLabel = new JLabel();
        txtHost = new javax.swing.JTextField(35);
        btConn = new javax.swing.JButton();
        btCancel = new javax.swing.JButton();

        jLabel.setText("主机地址：");
        btConn.setText("连接");
        txtHost.setText("127.0.0.1");
        btCancel.setText("取消");
        btConn.addActionListener(this);
        btCancel.addActionListener(this);

        JPanel first = new JPanel(new FlowLayout(FlowLayout.CENTER));
        first.add(jLabel);
        first.add(txtHost);

        JPanel second = new JPanel(new FlowLayout(FlowLayout.CENTER));
        second.add(btConn);
        second.add(btCancel);

        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new GridLayout(2, 1));
        contentPane.add(first);
        contentPane.add(second);

        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(parentFrame);

    }

    void setVisible() {
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btConn) {
            String host = txtHost.getText().trim();
            if (validateData(host)) {
                try {
                    ConnectData arg = new ConnectData();
                    arg.setHost(host);
                    arg.setPort(Constant.serverPort);
                    setConnArg(arg);
                    frame.setVisible(false);
                    JOptionPane.showMessageDialog(null, "请求已发送，等待目标主机确认", "提示", JOptionPane.INFORMATION_MESSAGE);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(frame, "Exception:" + ex.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        if (e.getSource() == btCancel) {
            frame.setVisible(false);
        }
    }

    private boolean validateData(String input) {
        boolean flag = false;
        if (input != null && !"".equals(input)) {
            flag = true;
        } else {
            JOptionPane.showMessageDialog(frame, "ERROR INPUT", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return flag;
    }

    private void setConnArg(ConnectData connArg) {
        setChanged();
        notifyObservers(connArg);
    }
}