package com.nevermore.remoter.remote;

import java.awt.event.InputEvent;
import java.rmi.Remote;
import java.rmi.RemoteException;


public interface RMIInterface extends Remote {
    /**
     * 获取第一帧屏幕图片
     */
    byte[] getImageData() throws RemoteException;

    /**
     * 执行屏幕操作命令
     */
    void executeEvent(InputEvent event) throws RemoteException;

    /**
     * 当服务器关闭服务时，本方法用来指示客户端不再向服务端获取屏幕图片
     */
    boolean isServerRunning() throws RemoteException;

}