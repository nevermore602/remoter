package com.nevermore.remoter.remote;

import com.nevermore.remoter.util.ZipUtils;
import com.nevermore.remoter.util.EventExecuter;
import com.nevermore.remoter.util.ScreenShot;

import java.awt.event.InputEvent;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;


public class RMIInterfaceImpl extends UnicastRemoteObject implements RMIInterface {

    private static final long serialVersionUID = 1L;
    private boolean serverRunning = false;

    public RMIInterfaceImpl() throws RemoteException {
        super();
    }

    @Override
    public byte[] getImageData() throws RemoteException {
        byte[] imgBytes = ScreenShot.getScreenShotData();
        return ZipUtils.zlib(imgBytes);
    }

    @Override
    public void executeEvent(InputEvent event) throws RemoteException {
        EventExecuter.execute(event);
    }

    @Override
    public boolean isServerRunning() throws RemoteException {
        return serverRunning;
    }

    public void setServerRunning(boolean flag) {
        serverRunning = flag;
    }

}
