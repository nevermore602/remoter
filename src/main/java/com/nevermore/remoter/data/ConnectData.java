package com.nevermore.remoter.data;

import lombok.Data;

@Data
public class ConnectData {
    private String host;
    private int port;
}
