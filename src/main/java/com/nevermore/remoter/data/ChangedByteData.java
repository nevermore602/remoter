package com.nevermore.remoter.data;

import lombok.Data;

import java.io.Serializable;

@Data
public class ChangedByteData implements Serializable {
    private int index;
    private byte data;
}
