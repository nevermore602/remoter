import com.nevermore.remoter.util.ZipUtils;
import com.nevermore.remoter.util.ScreenShot;

import java.io.IOException;

public class Test {

    @org.junit.Test
    public void testZip() throws InterruptedException, IOException {
        for (int i = 0; i < 1000000; i++) {
            byte[] bytes = ScreenShot.getScreenShotData();
            ZipUtils.gzip(bytes);
            ZipUtils.zlib(bytes);
        }
        Thread.sleep(100);
    }

    @org.junit.Test
    public void testZip2() throws Exception {
        //String str = "abcdefghijklmnopqrstuvwxyz";
        // String str = "aaaaaaaaaaaaaaaaaaaaabbbaaaaaaaaaaaaaacccaaaaddaaaaaaaaaaaaaaa";
        String str = "111111111111111111111111111111";
        byte[] bytes = str.getBytes();
        ZipUtils.gzip(bytes);
        ZipUtils.zlib(bytes);
    }
}
